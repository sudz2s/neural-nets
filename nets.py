import neuralnet
import numpy as np

class SingleLayerNet:
  def __init__(self, num_neuron, num_inputs, bias_flag = 0):
    self.num = num_neuron
    self.bias_flag = bias_flag
    self.num_inputs = num_inputs
    self.neurons = [] 
    for i in range(self.num):
      self.neurons.append(neuralnet.Neuron(self.num_inputs, self.bias_flag))
    self.initialize_weight_matrix()
    self.w_mat = self.obtain_weight_matrix()

  def obtain_weight_matrix(self):
    w_mat = []
    for inputs in range(self.num_inputs):
      w_mat.append([])
      for neuron in self.neurons:
        w_mat[inputs].append(neuron.weights[inputs])
    return w_mat

  def initialize_weight_matrix(self, weight_matrix = []):
    self.set_weight_matrix(weight_matrix)


  def set_weight_matrix(self, weight_matrix):
    if(self.bias_flag==0): num_inputs = self.num_inputs
    else: num_inputs = self.num_inputs + 1
    if(weight_matrix != []):   # change weight initialization
      w_mat_t = np.matrix(weight_matrix).transpose().tolist()
      print("Original", weight_matrix, "transpose", w_mat_t)
      for count in range(self.num):
        self.neurons[count].initialize_weights(w_mat_t[count])
        print(self.neurons[count].weights)
    else:
      self.w_mat = []
      for i in range(num_inputs):
        self.w_mat.append([])
        for j in range(self.num):
          self.w_mat[i].append(0)
    self.w_mat = self.obtain_weight_matrix()


  def initialize_bias(self, bias_list):
    i = 0
    for neuron in self.neurons:
      neuron.initialize_bias(bias_list[i])
      i = i + 1

  def add_bias_to_inputs(self, input_vector, neuron): 
    #for input_vector in input_matrix:
    input_vector.insert(0,neuron.bias_input)
    return(input_vector)  
    
  def update_weight(self, input_matrix, target_matrix):
    t_mat = np.matrix(target_matrix)
    if(self.bias_flag == 0): 
      s_mat = np.matrix(input_matrix)
      w_mat = s_mat.transpose() * t_mat
      return(w_mat.tolist())
    else: # need to fix the else part - logic to include bias with outer product weight calculation
      print("in matrix in else", input_matrix)
      sum_mat= np.zeros(shape=(len(input_matrix[0])+1,self.num),dtype=int)
      for count in range(input_matrix):
        input_vector = input_matrix[count]
        target_vector = target_matrix[count]
        for neuron in self.neurons:
          print("in vector before bias", input_vector)
          input_vector = self.add_bias_to_inputs(input_vector, neuron)
          #target_vector = t_mat[self.neurons.index(neuron)]
          print("in vector after bias addn", input_vector)
          s_mat = np.matrix(input_vector)
          w_mat = s_mat.transpose() * target_vector
          print("sum_mat, w_mat", sum_mat,w_mat)
        sum_mat = sum_mat + w_mat
      return(sum_mat.tolist())

  def train_network(self, input_matrix, target_matrix):
    w_mat = self.update_weight(input_matrix, target_matrix)
    self.set_weight_matrix(w_mat)
    

  def activation(self, net_matrix, threshold=0, fn_type=0):
    out_matrix = []
    count_pattern = 0
    for vector in net_matrix:
      out_matrix.append([])
      for value in vector:
        activation_val = neuralnet.Neuron.activation_fn(value, threshold, fn_type)
        out_matrix[count_pattern].append(activation_val)
      count_pattern = count_pattern + 1
    return out_matrix

  def test(self, input_vector):
    print("inside test fn")
    s_mat = np.matrix(input_vector)
    print(s_mat)
    w_mat = np.matrix(self.w_mat)
    print(w_mat)
    n_mat = s_mat * w_mat
    o_mat = self.activation(n_mat.tolist())
    return o_mat
    

if(__name__=="__main__"):
  net = SingleLayerNet(2, 4, 1)
  print(net.w_mat)
  #net.initialize_weight_matrix([[4,-4],[2,-2],[-2,2],[-4,4]])
  net.train_network([[1,-1,-1,-1],[1,1,-1,-1],[-1,-1,-1,1],[-1,-1,1,1]],[[1,-1],[1,-1],[-1,1],[-1,1]])
  print(net.w_mat) 
  print(net.test([[1,-1,-1,-1],[1,1,-1,-1],[-1,-1,-1,1],[-1,-1,1,1]]))
