# fn_type = 0 => bipolar threshold
#           1 => binary threshold 


class Neuron:
  
  def __init__(self, num_inputs, bias_flag = 0):
    self.num_inputs = num_inputs
    self.bias_flag = bias_flag
    self.num_ext_inputs = num_inputs
    self.weights = {}
    self.bias_input = 1
    if(bias_flag==1):   
      self.num_inputs = self.num_inputs + 1
      self.initialize_bias(0)
    for j in range(self.num_inputs):
      self.weights[j] = 0

  def initialize_bias(self, bias):
    self.weights[0] = bias
  
  def initialize_weights(self, weight_array):
    i = 0
    for weight in self.weights:
      if ((weight==0)and(self.bias_flag)): continue # bias
      self.weights[weight] = weight_array[i]
      i = i + 1

  def add_bias_to_input_matrix(self, input_matrix):
    for input_vector in input_matrix:
      input_vector.insert(0, self.bias_input)
      print("Inside add bias function", len(input_vector))
    return input_matrix

  def train_neuron(self, input_matrix, out_vector, lf = 1):
    if(self.bias_flag):
       input_matrix = self.add_bias_to_input_matrix(input_matrix)
    for i in range(len(input_matrix)):
      input_vector = input_matrix[i]
      net_value = self.summer_fn(input_vector)
      out_value = Neuron.activation_fn(net_value)
      self.update_weights(input_vector, out_vector[i], lf)


  def summer_fn(self, input_vector):
    i = 0
    sum_value = 0
    for weight in self.weights:
      sum_value = sum_value + (self.weights[weight]*input_vector[i])
      i = i + 1
    return sum_value


  def update_weights(self, training_vector, target_value, lf = 1):
    i = 0
    #print("Weights to be used for the below training vectors", self.weights)
    #print("Training vector", training_vector)
    #print("target value", target_value)
    for j in self.weights:
      #print("Current weight: ",self.weights[j], ", training vector input: ",training_vector[i], ", target value", target_value)
      self.weights[j] = lf * (self.weights[j] + (training_vector[i]*target_value))
      i = i + 1
      #print("weight update", self.weights)
     
  @classmethod 
  def activation_fn(cls, in_value, threshold=0, fn_type = 0):
    if(fn_type == 0):
      if(in_value > threshold):
        out_value = 1
      else:
        out_value = -1

    elif(fn_type == 1):
      if(in_value > threshold):
        out_value = 1
      else:
        out_value = 0

    elif(fn_type == 2):
      if(in_value > threshold):
        out_value = 1
      elif((in_value <= threshold) and (in_value >= -threshold)):
        out_value = 0
      else:
        out_value = -1
    #print("fn_type, net value, activation value", fn_type, in_value, out_value)
    return out_value

  def test(self, input_vector):
    if(self.bias_flag == 1):# and (self.bias_added == 0)):
      input_vector = self.add_bias_to_input_matrix([input_vector])[0]

    print("test",len(input_vector))
    print("weights", len(self.weights))
    net_value = self.summer_fn(input_vector)
    #print("test function net value: ", net_value)
    out_value = Neuron.activation_fn(net_value)
    return(out_value)
      

if(__name__=="__main__"):
  hebb_neuron = Neuron(2,0)
  weights = [0,0]
  hebb_neuron.initialize_weights(weights)
  input_matrix = [[1,1],[1,-1],[-1,1],[-1,-1]]
  target_matrix = [-1,-1,1,-1]
  hebb_neuron.train_neuron(input_matrix, target_matrix)
  print(hebb_neuron.weights)
  print(hebb_neuron.test([-1,-1]), hebb_neuron.test([-1,1]), hebb_neuron.test([1,-1]), hebb_neuron.test([1,1])) 
