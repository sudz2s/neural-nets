import neuralnet
import os
import copy
import random

class PerceptronNeuron(neuralnet.Neuron):
  def train_neuron(self, input_matrix, out_vector, threshold, lf):
    num_output = len(out_vector)
    for i in range(len(input_matrix)):
      input_vector = input_matrix[i]
      net_value = self.summer_fn(input_vector)
      out_value = neuralnet.Neuron.activation_fn(net_value, threshold, fn_type = 0)
      #print("In train neuron", i )
      if(out_value != out_vector[i%num_output]):
        self.update_weights(input_vector, out_vector[i%num_output], lf)

def getTrainingValues(path = "chartrainvectors.txt"):
  in_matrix = []
  out_matrix = []
  with open(path, "r") as infile:
    for line in infile.readlines():
      line = line.strip()
      [in_str, out_str] = line.split(":")
      in_str, out_str = list(in_str), list(out_str)
      for index in range(len(in_str)):
        in_str[index] = int(in_str[index]) - 1
      in_matrix.append(in_str)
      for index in range(len(out_str)):
        out_str[index] = int(out_str[index]) - 1
      out_matrix.append(out_str)
  #print(in_matrix, out_matrix, len(in_matrix), len(out_matrix))
  return in_matrix, out_matrix

class network:
  def __init__(self, input_matrix, target_matrix, threshold = 0, bias_flag = 0, lf = 1):
    self.num_in = len(input_matrix[0])
    self.num_out = len(target_matrix[0])
    self.bias_flag = bias_flag
    self.learn_rate = lf
    self.threshold = threshold
    self.neurons = []
    for i in range(self.num_out):
      self.neurons.append(PerceptronNeuron(self.num_in, self.bias_flag))
    self.trainNet(input_matrix, target_matrix)

  def trainNet(self, in_matrix, target_matrix):
    weight_vector = []
    for i in range(self.num_in):
      weight_vector.append(0)
    for perceptron_neuron in self.neurons:
      input_matrix = copy.deepcopy(in_matrix)
      perceptron_neuron.initialize_weights(weight_vector)
      print("neuron no. %s"%str(self.neurons.index(perceptron_neuron)) + " has %s weights" %str(len(perceptron_neuron.weights)))
      itercount = 0
      lf = 1
      if(self.bias_flag == 1): input_matrix = perceptron_neuron.add_bias_to_input_matrix(input_matrix)
      weight_vector = perceptron_neuron.weights.values()
      while(1):
        itercount = itercount + 1
        perceptron_neuron.train_neuron(input_matrix, target_matrix[self.neurons.index(perceptron_neuron)], threshold = self.threshold, lf = self.learn_rate)
        #print(weight_vector, perceptron_neuron.weights, "#############################################################################")
        if(perceptron_neuron.weights.values() == weight_vector): break
        weight_vector = perceptron_neuron.weights.values()
      print("Itercount", itercount, "##################################################################################3###################")

  def testNet(self, in_vector):
    out_vector = []
    for neuron in self.neurons:
       input_vector = copy.deepcopy(in_vector)
       #print("input", "weights", len(input_vector), len(neuron.weights))
       out_vector.append(neuron.test(input_vector))
    return out_vector

def getTrainingVectorFromGraphic(path = "testchar.txt"):
  in_vector = []
  with open(path, "r") as infile:
    for line in infile.readlines():
      line = line.strip()
      line = line.replace('#', '2')
      line = line.replace('.', '0')
      in_str =  list(line)
      for char in in_str:
        in_vector.append(int(char) - 1)
  return in_vector  

def testAllGraphicChars(pnet, path):
  out_matrix = []
  num_errors = 0
  charFiles = ["A.txt", "B.txt", "C.txt", "D.txt", "E.txt", "F.txt", "G.txt", "H.txt", "I.txt", "J.txt", "K.txt"]
  print(charFiles)
  for each in charFiles:
    #print(each)
    in_vector = getTrainingVectorFromGraphic(testPath + each)
    in_vector = generateErrors(in_vector, num_errors, mistake = 1, missing = 0)
    #print(in_vector, len(in_vector))
    out_vector = pnet.testNet(in_vector) 
    out_matrix.append(out_vector)
    #print( out_vector)
  return out_matrix


def generateErrors(in_vector, num, mistake = 1, missing = 0):
  for i in range(num):
    if(mistake):
      index = random.randint(0 ,len(in_vector) - 1)
      in_vector[index] = -(in_vector[index])
    if(missing):
      index = random.randint(0 ,len(in_vector) - 1)
      in_vector[index] = 0
  return in_vector

    
def getInputMatrixFromGraphic(testPath):
  in_matrix = []
  charFiles = os.listdir(testPath)
  for each in charFiles:
    in_matrix.append(getTrainingVectorFromGraphic(testPath + each))
  return in_matrix


def getNumberOfCorrectOutputs(mat1, mat2):
  index_count = []
  for index in range(len(mat1)):
    index_count.append(0)
  count = 0
  for index in range(len(mat1)):
    if(mat1[index] == mat2[index]):
      count = count + 1
    else: index_count[index] = index_count[index] + 1
    #else: print(mat2[index])
  return (count, index_count)
  

if(__name__=="__main__"):
  in_mat, out_mat = getTrainingValues()
  print("in_mat",len(in_mat), len(in_mat[0]))
  print("out_mat",len(out_mat), len(out_mat[0]))
  testPath = "/home/sud/neuralnets/testchars/"
  threshold = 0
  learnValues = []
  print("main fn no._in, no._out", len(in_mat[0]), len(out_mat[0]))
  while(threshold < 21):
    in_matrix = copy.deepcopy(in_mat)
    out_matrix = copy.deepcopy(out_mat[:len(out_mat[0])])
    print("main fn no._in, no._out", len(in_mat[0]), len(out_mat[0]))
    pnet = network(in_matrix, out_matrix, threshold, bias_flag = 0, lf = 1)
    threshold = threshold + 1
    summ = 0
    conf = []
    for i in range(len(out_mat[0])):
      conf.append(0)
    for i in range(1):
      actual_out_matrix = testAllGraphicChars(pnet, testPath)
      learn_number, neuron_confusion = getNumberOfCorrectOutputs(out_matrix, actual_out_matrix)
      print("ln,nc", learn_number, neuron_confusion)
      summ = summ + learn_number
      for j in range(len(neuron_confusion)):
        conf[j] = conf[j] + neuron_confusion[j]
    learnValues.append(summ/20)
    print("learn factor", learnValues)
  print("neuron confusion", conf)
