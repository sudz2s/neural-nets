#Neural Net library for Single Layer Perceptron Networks


neuralnet.py - a basic Neuron implemented as an Object (A Hebb neuron by default)

nets.py - uses neuralnet.py to create a Hebb network and calculates weight using outer product

perceptronNet.py - inherits the neuron class from neuralnet.py and customizes the update weight function to implement a perceptron neuron. Implements an SLP net for character (A-K) recognition

testchars/ - has sample characters in a 9x7 grid to test the character recognition SLP

chartrainvectors* - different training vectors for the above SLP. 
